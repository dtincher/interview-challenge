var express = require('express'),
    http = require('http');


var app = module.exports = express();

// map .renderFile to .html files
app.engine('html', require('ejs').renderFile);

// make .html the default
app.set('view engine', 'html');

// set views for pages
app.set('views', __dirname + '/views');

// serve static files
app.use(express.static(__dirname + '/public'));

app.get('/:id-tweets', function(req, res){
  
  var id = req.params.id;
  
  var options = {
    host: 'search.twitter.com',
    path: '/search.json?from=' + id
  };
  
  callback = function(response){
    var str = '';
    
    response.on('data', function(chunk) {
      str += chunk;
    });
    
    response.on('end', function() {
      
      res.status(200).render('tweets.html', {
         title: '@' + id + ' | Interview Challenge',
        handle: id,
        tweets: JSON.parse(str)
      });
      
    });
  };
  
  http.request(options, callback).end();
  
  //res.status(200).render('tweets.html', {
  //  title : '@' + id + ' | Interview Challenge',
  //  handle : id
  //});
  
});

if (!module.parent) {
  app.listen(30000);
  console.log('Express app started on port 30000.');
}