$(document).ready(function() {

  $('.tweet').each(function() {
    var tweet = $(this).html().split('</b>'),
        text = $.map(tweet[1].split(' '), function(element) {
          if (element.indexOf('http') !== -1) {
            element = '<a href="' + element + '">' + element + '</a>'; }
          return element;
        });

    tweet[1] = text.join(' ');

    $(this).html($.parseHTML(tweet.join('</b><br>')));
  });

});